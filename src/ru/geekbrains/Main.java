package ru.geekbrains;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        long startTime;
        int[] arr1 = makeArr(1000000, 1, 100);
        startTime = System.currentTimeMillis();
        int[] arr1sorted = mergeSort(arr1);
        System.out.println("Время сортировки слиянием: " + (System.currentTimeMillis() - startTime));

        int[] arr2 = makeArr(1000000, 1, 100);
        startTime = System.currentTimeMillis();
        int[] arr2sorted = shellSort(arr2);
        System.out.println("Время сортировки Шелла: " + (System.currentTimeMillis() - startTime));
    }

    /**
     * Задача 2
     * Реализовать сортировку Шелла.
     */
    private static int[] shellSort(int[] arr) {
        final int arrLen = arr.length;
        if (arrLen == 1) return arr;
        int inner, outer, tmp;
        int h = 1;
        while (h <= arrLen / 3) h = h * 3 + 1;
        while (h > 0) {
            for (outer = h; outer < arrLen; outer++) {
                tmp = arr[outer];
                inner = outer;
                while (inner > h - 1 && arr[inner - h] >= tmp) {
                    arr[inner] = arr[inner - h];
                    inner -= h;
                }
                arr[inner] = tmp;
            }
            h = (h - 1) / 3;
        }
        return arr;
    }

    /**
     * Задача 3
     * *Реализовать сортировку слиянием.
     */
    private static int[] mergeSort(int[] arr) {
        final int arrLen = arr.length;
        if (arrLen == 1) return arr;

        int middle = arrLen / 2;

        int[] part1 = new int[middle];
        System.arraycopy(arr, 0, part1, 0, part1.length);
        int[] part1sorted = mergeSort(part1);

        int[] part2 = new int[arrLen - middle];
        System.arraycopy(arr, middle, part2, 0, part2.length);
        int[] part2sorted = mergeSort(part2);

        return merge(part1sorted, part2sorted);
    }

    /**
     * Объединение двух массивов в один для сортировки слиянием.
     */
    private static int[] merge(int[] arr1, int[] arr2) {
        int index1 = 0;
        int index2 = 0;
        int sortedIndex = 0;
        final int arr1Len = arr1.length;
        final int arr2Len = arr2.length;
        int[] sorted = new int[arr1Len + arr2Len];
        for (int i = 0; i < sorted.length; i++) {
            if (index1 == arr1Len || index2 == arr2Len) break;
            sorted[sortedIndex++] = (arr1[index1] < arr2[index2]) ? arr1[index1++] : arr2[index2++];
        }
        while (index1 != arr1Len) {
            sorted[sortedIndex++] = arr1[index1++];
        }
        while (index2 != arr2Len) {
            sorted[sortedIndex++] = arr2[index2++];
        }
        return sorted;
    }

    /**
     * Гененирует массив заданной длины со значениями в диапазоне между min и max включительно.
     */
    private static int[] makeArr(int size, int min, int max) {
        final Random rand = new Random();
        int[] arr = new int[size];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = rand.nextInt((max - min) + 1) + min;
        }
        return arr;
    }
}
